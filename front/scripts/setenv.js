var _a = require('fs'), writeFile = _a.writeFile, existsSync = _a.existsSync, mkdirSync = _a.mkdirSync;
var argv = require('yargs').argv;
var dir = './src/environments';
require('dotenv').config();
var environment = argv.environment;
var isProduction = environment === 'prod';
var targetPath = isProduction
    ? "./src/environments/environment.prod.ts"
    : "./src/environments/environment.ts";
var environmentFileContent = "export const environment = {\n  production: ".concat(isProduction, ",\n  API_URL: \"").concat(process.env['API_URL'], "\",\n  STORAGE_URL: \"").concat(process.env['STORAGE_URL'], "\",\n};");
if (!existsSync(dir)) {
    mkdirSync(dir);
}
writeFile(targetPath, environmentFileContent, function (err) {
    if (err)
        console.log(err);
    console.log("Wrote variables to ".concat(targetPath));
});
