import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../shared/store/type';
import { Category } from '../../shared/models/category.model';
import { fetchCategoriesRequest } from '../../shared/store/categories/categories.actions';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.sass']
})
export class PriceComponent implements OnInit, OnDestroy {
  categories: Observable<null | Category[]>;
  categoriesSub!: Subscription;
  categoriesData!: null | Category[];

  fetchLoading: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.categories = store.select(state => state.categories.items);
    this.fetchLoading = store.select(state => state.categories.fetchLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCategoriesRequest());
    this.categoriesSub = this.categories.subscribe(data => {
      this.categoriesData = data;
    });
  }

  ngOnDestroy(): void {
    this.categoriesSub.unsubscribe();
  }

}
