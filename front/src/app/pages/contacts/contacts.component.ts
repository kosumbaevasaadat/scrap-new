import { Observable, Subscription } from 'rxjs';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Contact } from '../../shared/models/contact.model';
import { AppState } from '../../shared/store/type';
import { fetchContactsRequest } from '../../shared/store/contacts/contacts.actions';
import { Callback } from '../../shared/models/callback.model';
import { createCallbackRequest } from '../../shared/store/callbacks/callbacks.actions';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.sass']
})
export class ContactsComponent {
  contacts: Observable<null | Contact[]>;
  contactsSub!: Subscription;
  contactsData!: null | Contact[];

  name = '';
  phone = '';

  constructor(private store: Store<AppState>) {
    this.contacts = store.select(state => state.contacts.items);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchContactsRequest());
    this.contactsSub = this.contacts.subscribe(data => {
      this.contactsData = data;
    });
  }

  onPhoneInput() {
    const txt = this.phone;
    if (txt.length == 3 || txt.length == 7 || txt.length == 10)
      this.phone = this.phone + ' ';
  }

  onSubmit() {
    const data: Callback = {
      name: this.name,
      phone: this.phone,
    }
    this.store.dispatch(createCallbackRequest({ data }));
    this.name = '';
    this.phone = '';
  }

  ngOnDestroy(): void {
    this.contactsSub.unsubscribe();
  }

}
