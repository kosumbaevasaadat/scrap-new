import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';

import { categoriesReducer } from './store/categories/categories.reducer';
import { CategoriesEffects } from './store/categories/categories.effects';
import { contactsReducer } from './store/contacts/contacts.reducer';
import { ContactsEffects } from './store/contacts/contacts.effects';
import { CallbacksEffects } from './store/callbacks/callbacks.effects';
import { callbacksReducer } from './store/callbacks/callbacks.reducer';
import { exportsReducer } from './store/exports/exports.reducer';
import { ExportsEffects } from './store/exports/exports.effects';

const reducers = {
  categories: categoriesReducer,
  contacts: contactsReducer,
  callbacks: callbacksReducer,
  exports: exportsReducer,
};

const effects = [
  CategoriesEffects,
  ContactsEffects,
  CallbacksEffects,
  ExportsEffects,
];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule],
})
export class AppStoreModule {
}
