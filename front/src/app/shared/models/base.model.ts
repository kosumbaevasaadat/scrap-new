export interface BaseModel {
  id: number,
  createDate: Date,
  updateDate: Date,
}
