import { BaseModel } from './base.model';

export interface Price extends BaseModel {
  title: string,
  price: number,
  unit: string,
}
