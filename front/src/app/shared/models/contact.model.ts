import { BaseModel } from './base.model';

export interface Contact extends BaseModel {
  address: string,
  phone: string,
  schedule: string,
  email: string,
}
