import { BaseModel } from './base.model';
import { Price } from './price.model';

export interface Category extends BaseModel {
  title: string,
  prices: Price[],
}
