import { catchError, map, mergeMap, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { ContactsService } from '../../services/contacts.service';
import { fetchContactsFailure, fetchContactsRequest, fetchContactsSuccess } from './contacts.actions';

@Injectable()
export class ContactsEffects {
  constructor(
    private actions: Actions,
    private contactsService: ContactsService
  ) {
  }

  fetchContacts = createEffect(() => this.actions.pipe(
    ofType(fetchContactsRequest),
    mergeMap(() => this.contactsService.getAll().pipe(
      map(items => fetchContactsSuccess({ items })),
      catchError(() => {
        return of(fetchContactsFailure());
      }),
    )),
  ));
}
