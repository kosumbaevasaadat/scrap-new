import { createReducer, on } from '@ngrx/store';
import { ContactsState } from '../type';
import { fetchContactsFailure, fetchContactsRequest, fetchContactsSuccess } from './contacts.actions';

const initialState: ContactsState = {
  items: null,
  fetchLoading: false,
};

export const contactsReducer = createReducer(
  initialState,
  on(fetchContactsRequest, state => ({ ...state, fetchLoading: true })),
  on(fetchContactsSuccess, (state, { items }) => ({ ...state, fetchLoading: false, items })),
  on(fetchContactsFailure, state => ({ ...state, fetchLoading: false })),
);
