import { createAction, props } from '@ngrx/store';
import { Contact } from '../../models/contact.model';

export const fetchContactsRequest = createAction('[Contacts] Fetch Request');
export const fetchContactsSuccess = createAction('[Contacts] Fetch Success', props<{items: null | Contact[]}>());
export const fetchContactsFailure = createAction('[Contacts] Fetch Failure');
