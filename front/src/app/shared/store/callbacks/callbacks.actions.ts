import { createAction, props } from '@ngrx/store';
import { Callback } from '../../models/callback.model';

export const createCallbackRequest = createAction('[Callback] Create Request', props<{ data: Callback }>());
export const createCallbackSuccess = createAction('[Callback] Create Success');
export const createCallbackFailure = createAction('[Callback] Create Failure');
