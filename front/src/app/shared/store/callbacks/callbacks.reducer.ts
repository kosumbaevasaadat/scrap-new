import { CallbacksState } from '../type';
import { createReducer, on } from '@ngrx/store';
import { createCallbackFailure, createCallbackRequest, createCallbackSuccess } from './callbacks.actions';

const initialState: CallbacksState = {
  createLoading: false,
};

export const callbacksReducer = createReducer(
  initialState,
  on(createCallbackRequest, state => ({...state, createLoading: true})),
  on(createCallbackSuccess, state => ({...state, createLoading: false})),
  on(createCallbackFailure, state => ({...state, createLoading: false})),
)
