import { catchError, map, mergeMap, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { CallbacksService } from '../../services/callbacks.service';
import { createCallbackFailure, createCallbackRequest, createCallbackSuccess } from './callbacks.actions';

@Injectable()
export class CallbacksEffects {
  constructor(private actions: Actions,
              private callbacksService: CallbacksService) {
  }

  createCallback = createEffect(() => this.actions.pipe(
    ofType(createCallbackRequest),
    mergeMap(({ data }) => this.callbacksService.create(data).pipe(
      map(() => createCallbackSuccess()),
      catchError(() => {
        return of(createCallbackFailure());
      }),
    )),
  ));

}
