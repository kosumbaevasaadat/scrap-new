import { Category } from '../models/category.model';
import { Contact } from '../models/contact.model';

export type CategoriesState = {
  items: null | Category[],
  fetchLoading: boolean,
};

export type ContactsState = {
  items: null | Contact[],
  fetchLoading: boolean,
};

export type CallbacksState = {
  createLoading: boolean,
};

export type ExportsState = {
  createLoading: boolean,
};

export type AppState = {
  categories: CategoriesState,
  contacts: ContactsState,
  callbacks: CallbacksState,
};
