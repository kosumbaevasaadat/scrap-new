import { createReducer, on } from '@ngrx/store';

import { CategoriesState } from '../type';
import { fetchCategoriesFailure, fetchCategoriesRequest, fetchCategoriesSuccess } from './categories.actions';

const initialState: CategoriesState = {
  items: null,
  fetchLoading: false,
};

export const categoriesReducer = createReducer(
  initialState,
  on(fetchCategoriesRequest, state => ({ ...state, fetchLoading: true })),
  on(fetchCategoriesSuccess, (state, { items }) => ({ ...state, fetchLoading: false, items })),
  on(fetchCategoriesFailure, state => ({ ...state, fetchLoading: false })),
);
