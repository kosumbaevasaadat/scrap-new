import { createAction, props } from '@ngrx/store';
import { Export } from '../../models/export.model';

export const createExportRequest = createAction('[Export] Create Request', props<{data: Export}>());
export const createExportSuccess = createAction('[Export] Create Request');
export const createExportFailure = createAction('[Export] Create Request');
