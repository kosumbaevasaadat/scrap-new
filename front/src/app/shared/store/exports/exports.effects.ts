import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ExportsService } from '../../services/exports.service';
import { createExportFailure, createExportRequest, createExportSuccess } from './exports.actions';
import { catchError, map, mergeMap, of } from 'rxjs';

@Injectable()
export class ExportsEffects {
  constructor(private actions: Actions,
              private exportsService: ExportsService) {
  }

  createExport = createEffect(() => this.actions.pipe(
    ofType(createExportRequest),
    mergeMap(({ data }) => this.exportsService.create(data)),
    map(() => createExportSuccess()),
    catchError(() => {
      return of(createExportFailure());
    }),
  ));

}
