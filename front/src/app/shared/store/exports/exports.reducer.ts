import { createReducer, on } from '@ngrx/store';

import { ExportsState } from '../type';
import { createExportFailure, createExportRequest, createExportSuccess } from './exports.actions';

const initialState: ExportsState = {
  createLoading: false,
};

export const exportsReducer = createReducer(
  initialState,
  on(createExportRequest, state => ({ ...state, createLoading: true })),
  on(createExportSuccess, state => ({ ...state, createLoading: false })),
  on(createExportFailure, state => ({...state, createLoading: false})),
);
