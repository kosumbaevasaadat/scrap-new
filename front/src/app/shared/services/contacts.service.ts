import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Contact } from '../models/contact.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<Contact[]>(env.API_URL + '/contacts')
  }
}
