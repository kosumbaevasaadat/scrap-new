import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Export } from '../models/export.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExportsService {
  constructor(private http: HttpClient) {
  }

  create(data: Export) {
    return this.http.post(env.API_URL + '/export', data);
  }
}
