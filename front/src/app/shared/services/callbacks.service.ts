import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Callback } from '../models/callback.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CallbacksService {
  constructor(private http: HttpClient) {
  }

  create(data: Callback) {
    return this.http.post(env.API_URL + '/callback', data);
  }
}
