import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { MainInfoComponent } from './ui/main-info/main-info.component';
import { ServicesComponent } from './ui/services/services.component';
import { ExportComponent } from './ui/export/export.component';
import { PriceComponent } from './pages/price/price.component';
import { CallbackComponent } from './ui/callback/callback.component';
import { FooterComponent } from './ui/footer/footer.component';
import { CallbackModalComponent } from './ui/callback-modal/callback-modal.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { AppStoreModule } from './shared/app-store.module';
import { ValidatePhoneDirective } from './shared/directives/validate-phone.directive';
import { ExportModalComponent } from './ui/export-modal/export-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    MainInfoComponent,
    ServicesComponent,
    ExportComponent,
    PriceComponent,
    CallbackComponent,
    FooterComponent,
    CallbackModalComponent,
    AboutComponent,
    ContactsComponent,
    ValidatePhoneDirective,
    ExportModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    AppStoreModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
