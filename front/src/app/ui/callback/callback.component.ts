import { Store } from '@ngrx/store';
import { Component, ElementRef, ViewChild } from '@angular/core';

import { Callback } from '../../shared/models/callback.model';
import { createCallbackRequest } from '../../shared/store/callbacks/callbacks.actions';
import { AppState } from '../../shared/store/type';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.sass']
})
export class CallbackComponent {
  @ViewChild('phoneInput', { static: true }) phoneInput!: ElementRef;
  name = '';
  phone = '';

  constructor(private store: Store<AppState>) {
  }

  isDisabled() {
    return this.name === '' || this.phone === '';
  }

  onPhoneInput() {
    const txt = this.phone;
    if (txt.length == 3 || txt.length == 7 || txt.length == 10)
      this.phone = this.phone + ' ';
  }

  onSubmit() {
    const data: Callback = {
      name: this.name,
      phone: this.phone,
    }
    this.store.dispatch(createCallbackRequest({ data }));
    this.name = '';
    this.phone = '';
  }

}
