import { Observable, Subscription } from 'rxjs';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Contact } from '../../shared/models/contact.model';
import { AppState } from '../../shared/store/type';
import { fetchContactsRequest } from '../../shared/store/contacts/contacts.actions';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent {
  contacts: Observable<null | Contact[]>;
  contactsSub!: Subscription;
  contactsData!: null | Contact[];

  constructor(private store: Store<AppState>) {
    this.contacts = store.select(state => state.contacts.items);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchContactsRequest());
    this.contactsSub = this.contacts.subscribe(data => {
      this.contactsData = data;
    });
  }

  ngOnDestroy(): void {
    this.contactsSub.unsubscribe();
  }

}
