import { Component, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';

import { Callback } from '../../shared/models/callback.model';
import { AppState } from '../../shared/store/type';
import { createCallbackRequest } from '../../shared/store/callbacks/callbacks.actions';

@Component({
  selector: 'app-callback-modal',
  templateUrl: './callback-modal.component.html',
  styleUrls: ['./callback-modal.component.sass']
})
export class CallbackModalComponent {
  @ViewChild('nameInput', { static: true }) nameInput!: ElementRef;
  @ViewChild('phoneInput', { static: true }) phoneInput!: ElementRef;
  name = '';
  phone = '';
  checkInput = false;

  nameTouched = false;
  phoneTouched = false;

  constructor(private store: Store<AppState>) {
  }

  onNameInput() {
    this.nameTouched = true;
  }

  onPhoneInput() {
    this.phoneTouched = true;
    const txt = this.phone;
    if (txt.length == 3 || txt.length == 7 || txt.length == 10)
      this.phone = this.phone + ' ';
  }

  addClass() {
    const nameInputElement = this.nameInput.nativeElement;
    const nameIsTouched = nameInputElement.classList.contains('ng-untouched');

    const phoneInputElement = this.phoneInput.nativeElement;
    const phoneIsTouched = phoneInputElement.classList.contains('ng-untouched');

    if (nameIsTouched || phoneIsTouched) {
      return this.name === '' || this.phone === '' ? 'was-validated' : '';
    }
    return '';
  }

  onSubmit() {
    const data: Callback = {
      name: this.name,
      phone: this.phone,
    }
    this.store.dispatch(createCallbackRequest({ data }));
  }

  isDisabled() {
    return this.name === '' || this.phone === '' || !this.checkInput;
  }

}
