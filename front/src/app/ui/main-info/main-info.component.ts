import { Component } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Contact } from '../../shared/models/contact.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../shared/store/type';
import { fetchContactsRequest } from '../../shared/store/contacts/contacts.actions';

@Component({
  selector: 'app-main-info',
  templateUrl: './main-info.component.html',
  styleUrls: ['./main-info.component.sass']
})
export class MainInfoComponent {
  contacts: Observable<null | Contact[]>;
  contactsSub!: Subscription;
  contactsData!: null | Contact[];

  constructor(private store: Store<AppState>) {
    this.contacts = store.select(state => state.contacts.items);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchContactsRequest());
    this.contactsSub = this.contacts.subscribe(data => {
      this.contactsData = data;
    });
  }

  ngOnDestroy(): void {
    this.contactsSub.unsubscribe();
  }
}
