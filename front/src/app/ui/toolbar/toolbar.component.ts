import { Observable, Subscription } from 'rxjs';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Contact } from '../../shared/models/contact.model';
import { AppState } from '../../shared/store/type';
import { fetchContactsRequest } from '../../shared/store/contacts/contacts.actions';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.sass']
})
export class ToolbarComponent {
  contacts: Observable<null | Contact[]>;
  contactsSub!: Subscription;
  contactsData!: null | Contact[];

  constructor(private store: Store<AppState>) {
    this.contacts = store.select(state => state.contacts.items);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchContactsRequest());
    this.contactsSub = this.contacts.subscribe(data => {
      this.contactsData = data;
    });
  }

  openMenu() {
    const menu = document.getElementById('mainNav');
    if (menu) {
      menu.classList.toggle('open-menu');
    }
  }

  ngOnDestroy(): void {
    this.contactsSub.unsubscribe();
  }

}
