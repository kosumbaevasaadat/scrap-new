import { Component, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../shared/store/type';
import { Export } from '../../shared/models/export.model';
import { createExportRequest } from '../../shared/store/exports/exports.actions';

@Component({
  selector: 'app-export-modal',
  templateUrl: './export-modal.component.html',
  styleUrls: ['./export-modal.component.sass']
})
export class ExportModalComponent {
  @ViewChild('nameInput', { static: true }) nameInput!: ElementRef;
  @ViewChild('phoneInput', { static: true }) phoneInput!: ElementRef;
  name = '';
  phone = '';
  checkInput = false;

  nameTouched = false;
  phoneTouched = false;

  constructor(private store: Store<AppState>) {
  }

  onNameInput() {
    this.nameTouched = true;
  }

  onPhoneInput() {
    this.phoneTouched = true;
    const txt = this.phone;
    if (txt.length == 3 || txt.length == 7 || txt.length == 10)
      this.phone = this.phone + ' ';
  }

  addClass() {
    const nameInputElement = this.nameInput.nativeElement;
    const nameIsTouched = nameInputElement.classList.contains('ng-untouched');

    const phoneInputElement = this.phoneInput.nativeElement;
    const phoneIsTouched = phoneInputElement.classList.contains('ng-untouched');

    if (nameIsTouched || phoneIsTouched) {
      return this.name === '' || this.phone === '' ? 'was-validated' : '';
    }
    return '';
  }

  onSubmit() {
    const data: Export = {
      name: this.name,
      phone: this.phone,
    }
    this.store.dispatch(createExportRequest({ data }));
  }

  isDisabled() {
    return this.name === '' || this.phone === '' || !this.checkInput;
  }
}
