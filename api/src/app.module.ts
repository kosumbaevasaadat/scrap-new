import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { dataSourceOptions } from './db/data-source';
import { CategoryModule } from './modules/category/category.module';
import { ContactsModule } from './modules/contacts/contacts.module';
import { CallbackModule } from './modules/callback/callback.module';
import { ExportModule } from './modules/export/export.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOptions),
    ConfigModule.forRoot({ isGlobal: true }),
    CategoryModule,
    ContactsModule,
    CallbackModule,
    ExportModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
