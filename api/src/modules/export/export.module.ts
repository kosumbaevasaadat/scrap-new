import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ExportService } from './export.service';
import { ExportController } from './export.controller';
import { Export } from '../../entities/export.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Export])],
  controllers: [ExportController],
  providers: [ExportService],
  exports: [ExportService],
})
export class ExportModule {}
