import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateExportDto } from './dto/create-export.dto';
import { Export } from '../../entities/export.entity';

@Injectable()
export class ExportService {
  constructor(
    @InjectRepository(Export)
    private exportRepository: Repository<Export>,
  ) {
  }

  async create(createExportDto: CreateExportDto) {
    await this.exportRepository.save(
      this.exportRepository.create({...createExportDto, status: 'Новый'}),
    );
    return {message: 'Создано успешно!'};
  }
}
