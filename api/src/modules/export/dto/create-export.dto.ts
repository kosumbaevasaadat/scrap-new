import { IsNotEmpty, IsString } from 'class-validator';

export class CreateExportDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  phone: string;
}
