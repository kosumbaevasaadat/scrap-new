import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CallbackService } from './callback.service';
import { CallbackController } from './callback.controller';
import { Callback } from '../../entities/callback.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Callback])],
  controllers: [CallbackController],
  providers: [CallbackService],
  exports: [CallbackService],
})
export class CallbackModule {
}
