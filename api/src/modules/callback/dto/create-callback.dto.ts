import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCallbackDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  phone: string;
}
