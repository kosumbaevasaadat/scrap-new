import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { CallbackService } from './callback.service';
import { CreateCallbackDto } from './dto/create-callback.dto';

@Controller('callback')
export class CallbackController {
  constructor(private readonly callbackService: CallbackService) {
  }

  @HttpCode(201)
  @Post()
  create(@Body() createCallbackDto: CreateCallbackDto) {
    return this.callbackService.create(createCallbackDto);
  }

}
