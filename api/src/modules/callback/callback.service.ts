import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateCallbackDto } from './dto/create-callback.dto';
import { Callback } from '../../entities/callback.entity';

@Injectable()
export class CallbackService {
  constructor(
    @InjectRepository(Callback)
    private callbackRepository: Repository<Callback>
  ) {
  }

  async create(createCallbackDto: CreateCallbackDto) {
    const callback = await this.callbackRepository.create({ ...createCallbackDto, status: 'Новый' });
    await this.callbackRepository.save(callback);
    return {message: 'Создано успешно!'};
  }
}
