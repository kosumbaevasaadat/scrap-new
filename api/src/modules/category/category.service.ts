import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Category } from '../../entities/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>
  ) {
  }

  findAll() {
    return this.categoryRepository.createQueryBuilder('category')
      .leftJoinAndSelect('category.prices', 'price')
      .getMany();
  }

}
