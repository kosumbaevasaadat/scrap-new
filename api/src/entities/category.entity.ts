import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Price } from './price.entity';

@Entity()
export class Category extends BaseEntity {
  @Column()
  title: string;

  @OneToMany(() => Price, price => price.category)
  prices: Price[];
}