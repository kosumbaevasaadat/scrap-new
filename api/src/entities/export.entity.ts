import { Column, Entity } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class Export extends BaseEntity {
  @Column()
  phone: string;

  @Column()
  name: string;

  @Column()
  status: string;
}