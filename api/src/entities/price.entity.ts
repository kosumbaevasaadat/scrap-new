import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Category } from './category.entity';

@Entity()
export class Price extends BaseEntity {
  @Column()
  title: string;

  @Column({type: 'int'})
  price: number;

  @Column()
  unit: string;

  @ManyToOne(() => Category, category => category.prices)
  category: Category;
}