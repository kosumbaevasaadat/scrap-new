import { Column, Entity } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class Contacts extends BaseEntity {
  @Column()
  address: string;

  @Column()
  phone: string;

  @Column()
  schedule: string;

  @Column()
  email: string;
}