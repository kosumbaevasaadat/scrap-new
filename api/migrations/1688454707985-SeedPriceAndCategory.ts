import { MigrationInterface, QueryRunner } from 'typeorm'
import { Category } from '../src/entities/category.entity';
import { Price } from '../src/entities/price.entity';


export class SeedPriceAndCategory1688454707985 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const [
      blackMetal,
      nonFerrousMetal,
    ] = await queryRunner.manager.save(
      queryRunner.manager.create<Category>(Category,
        [
          {
            title: 'Черный металл',
          },
          {
            title: 'Цветной металл',
          },
        ]),
    );

    await queryRunner.manager.save(
      queryRunner.manager.create<Price>(Price,
        [
          {
            title: 'Лом черных металлов – все категории',
            price: 22000,
            unit: 'руб/тонна',
            category: blackMetal,
          }, {
          title: 'Медь',
          price: 610,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Медный кабель',
          price: 140,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Алюминий',
          price: 130,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Алюминиевый кабель',
          price: 30,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Латунь',
          price: 300,
          unit: 'руб/тонна',
          category: nonFerrousMetal,
        }, {
          title: 'Бронза',
          price: 300,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Нержавейка',
          price: 75,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Свинец',
          price: 90,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'Плавленый свинец',
          price: 90,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        }, {
          title: 'АКБ (аккумуляторы)',
          price: 50,
          unit: 'руб/кг',
          category: nonFerrousMetal,
        },
        ]),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const categoryTitlesToDelete = [
      'Черный металл',
      'Цветной металл',
    ];
    await queryRunner.query(
      `DELETE FROM category WHERE title IN (${categoryTitlesToDelete.map(title => `'${title}'`).join(', ')})`
    );

    const priceTitlesToDelete = [
      'Лом черных металлов – все категории',
      'Медь',
      'Медный кабель',
      'Алюминий',
      'Алюминиевый кабель',
      'Латунь',
      'Бронза',
      'Нержавейка',
      'Свинец',
      'Плавленый свинец',
      'АКБ (аккумуляторы)',
    ];
    await queryRunner.query(
      `DELETE FROM price WHERE title IN (${priceTitlesToDelete.map(title => `'${title}'`).join(', ')})`
    );
  }

}
