import { MigrationInterface, QueryRunner } from 'typeorm'
import { Contacts } from '../src/entities/contacts.entity';

export class SeedContacts1688468228937 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<Contacts>(Contacts, {
        address: 'Санкт-Петербург, г. Пушкин, Красносельское шоссе 14/28',
        phone: '+7 (921) 302-32-22',
        schedule: 'Ежедневно (9.00 - 18.00)',
        email: 'info@scrapline.ru',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM contacts WHERE phone = '+7 (921) 302-32-22'`);
  }

}
